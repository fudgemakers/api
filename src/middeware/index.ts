import { NextFunction, Request, Response, RequestHandler } from 'express';
import { HttpException } from '../exceptions';
import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';

const errorMiddleware = (error: HttpException, _request: Request, response: Response, _next: NextFunction) => {
  const status = error.status || 500;
  const message = error.message || 'Something went wrong';
  response
    .status(status)
    .send({
      status,
      message,
    })
}

const validationMiddleware = <T>(type: any, skipMissingProperties = false): RequestHandler => {
  return (req, res, next) => {
    validate(plainToClass(type, req.body), { skipMissingProperties })
      .then((errors: ValidationError[]) => {
        if (errors.length > 0) {
          const message = errors.map((error: ValidationError) => Object.values(error.constraints ?? '')).join(', ');
          next(new HttpException(400, message));
        } else {
          next();
        }
      });
  };
}

export { errorMiddleware, validationMiddleware };