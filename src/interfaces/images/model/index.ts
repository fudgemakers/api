import { BuildOptions, DataTypes, Model, Sequelize } from 'sequelize';

export interface ImageAttributes {
  lotId: number;
  publicId: string;
  width: number;
  height: number;
  type: string;
  tags: string;
  url: string;
  secureUrl: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface CloudinaryUpload {
  folder: string;
  tags: string[];
  context: string;
}

export interface CloudinaryUploadResponse {
  asset_id: string;
  public_id: string;
  version: number;
  version_id: string;
  signature: string;
  width: number;
  height: number;
  format: string;
  resource_type: string;
  created_at: Date;
  tags: string[],
  bytes: number;
  type: string;
  etag: string;
  placeholder: boolean;
  url: string;
  secure_url: string;
  original_filename: string;
}

export interface ImagesModel extends Model<ImageAttributes>, ImageAttributes {};
export class Images extends Model<ImagesModel, ImageAttributes> {};

export type ImagesStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): ImagesModel;
};

export const ImagesFactory = (sequelize: Sequelize): ImagesStatic => {
  return sequelize.define('images', {
    id: {
      type: new DataTypes.INTEGER(),
      autoIncrement: true,
      primaryKey: true,
    },
    lotId: {
      type: new DataTypes.INTEGER(),
      allowNull: false
    },
    publicId: {
      type: new DataTypes.STRING(),
      allowNull: false
    },
    width: {
      type: new DataTypes.INTEGER(),
      allowNull: false
    },
    height: {
      type: new DataTypes.INTEGER(),
      allowNull: false
    },
    type: {
      type: new DataTypes.STRING(),
      allowNull: false
    },
    tags: {
      type: new DataTypes.STRING(),
      allowNull: true
    },
    url: {
      type: new DataTypes.STRING(),
      allowNull: false
    },
    secureUrl: {
      type: new DataTypes.STRING(),
      allowNull: false
    }
  },
  {
    tableName: 'images'
  }) as ImagesStatic;
}
