import express, { NextFunction } from 'express';
import multer from 'multer';
import { cloudinaryConfig } from '../../../config';
import { PostNotFoundException } from '../../../exceptions';
import { ImagesModel } from '../../lot/model';
import { CloudinaryUpload, CloudinaryUploadResponse, ImageAttributes } from '../model';

const cloudinary = require('cloudinary').v2;
const streamifier = require('streamifier');

cloudinary.config(cloudinaryConfig);

class ImagesController {
  public path = '/images';
  public router = express.Router();

  constructor() {
    this.router.get(`${this.path}/:id`, this.findOne);
    this.router.post(`${this.path}/upload`, multer().single('image'), this.upload);
    this.router.delete(`${this.path}/:id`, this.deleteOne);
  }

  upload = (req: express.Request, res: express.Response): void => {

    const artist = req.body.artist as string | undefined ?? '';

    const streamUpload = (req: express.Request): Promise<CloudinaryUploadResponse | string> => {
      return new Promise((resolve, reject) => {
        const stream = cloudinary.uploader.upload_stream(
          {
            folder: 'foo',
            tags: [artist],
            context: `alt=${artist}❘caption=${artist}`,
          } as CloudinaryUpload,
          (error: string, result: CloudinaryUploadResponse) => {
            if (result) {
              resolve(result);
            } else {
              reject(error);
            }
          }
        );
        streamifier.createReadStream(req.file.buffer).pipe(stream);
      });
    };
  
    async function uploadAndSaveToDb(req: express.Request): Promise<void> {
      const result = await streamUpload(req);

      if (typeof result === 'object') {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        const { public_id, width, height, type, tags, url, secure_url } = result;
        const { lotId } = req.body;

        const image = {
          lotId,
          publicId: public_id,
          width,
          height,
          type,
          tags: tags.join(','),
          url,
          secureUrl: secure_url
        } as ImageAttributes;

        ImagesModel.create(image)
          .then((data: any) => {
            res.send(data);
          })
          .catch((err: { message: any; }) => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating the Image."
            });
          });
      }
    }

    uploadAndSaveToDb(req);

  };


    // Find an image with an id
  findOne = (req: express.Request, res: express.Response, next: NextFunction): void => {
    const id = req.params.id;

    ImagesModel.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          next(new PostNotFoundException(id));
        }

      })
      .catch(error => {
        res.status(500).send({
          message: `Error ${ error }`
        });
      });
  };

    // Delete an Image with the specified id in the request
  deleteOne = (req: express.Request, res: express.Response): void => {
    const id = req.params.id;

    ImagesModel.destroy({
      where: { id }
    })
      .then(num => {
        if (num === 1) {
          res.send({
            message: `Image was deleted successfully!`
          });
        } else {
          res.send({
            message: `Cannot delete Image with id ${ id }. Maybe Image was not found!`
          });
        }
      })
      .catch(error => {
        res.status(500).send({
          message: `Error ${ error }`
        });
      });
  };

}

export default ImagesController;