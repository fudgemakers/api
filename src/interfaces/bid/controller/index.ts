import express, { NextFunction } from 'express';
import { Op } from 'sequelize';
import { PostNotFoundException } from '../../../exceptions';
import { validationMiddleware } from '../../../middeware';
import { BidModel } from '../../lot/model';
import CreateBid from '../tdo';


class BidController {
  public path = '/bid';
  public router = express.Router();

  constructor() {
    this.router.get(this.path, this.findAll);
    this.router.get(`${this.path}/:id`, this.findOne);
    this.router.post(this.path, validationMiddleware(CreateBid), this.create);
    this.router.patch(`${this.path}/:id`, validationMiddleware(CreateBid, true), this.update);
    this.router.delete(`${this.path}/:id`, this.deleteOne);
  }

  create = (req: express.Request, res: express.Response) => {

    BidModel.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch((error: { message: string; }) => {
        res.status(500).send({
          message:
            error.message || "Some error occurred while creating the Lot."
        });
      });
  };

  findAll = (req: express.Request, res: express.Response) => {
    const conditions = req.query ?
      Object.entries(req.query).map(([key, value]) => {
        const c = (key === 'bid' || key === 'lotid') ? { [Op.eq]: `${value}` } : { [Op.like]: `%${value}%` };
        return { [key]: c };
      }) : null;
    const flattened = conditions ? conditions.map(c => c) : {};
    BidModel.findAll({ where: flattened })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Lots."
        });
      });
  };

  private findOne = (req: express.Request, res: express.Response, next: NextFunction) => {

    const { id } = req.params;

    BidModel.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          next(new PostNotFoundException(id));
        }
      })
      .catch(error => {
        res.status(500).send({
          status: 'error',
          message: `Error ${ error }`
        });
      });
  };

  private update = (req: express.Request, res: express.Response, next: NextFunction) => {
    const { id } = req.params;
    if (!id) next(new PostNotFoundException(id));

    BidModel.update(req.body, {
      where: { id }
      })
      .then(data => {
        if (data[0] === 1) {
          res.send({
            status: 'ok'
          });
        } else {
          next(new PostNotFoundException(id));
        }
      })
      .catch(error => {
        res.status(500).send({
          message: `Error ${ error}`
        });
      });
  };

  private deleteOne = (req: express.Request, res: express.Response, next: NextFunction) => {
    const { id } = req.params;

    if (!id) next(new PostNotFoundException(id));

    BidModel.destroy({
      where: { id }
    })
      .then(data => {
        if (data === 1) {
          res.send({
            status: 'ok',
            message: `Bid ${ id } deleted successfully`
          });
        } else {
          res.send({
            status: 'error',
            message: `Cannot delete bid ${id}.`
          });
        }
      })
      .catch(error => {
        res.status(500).send({
          message: `Error ${ error }`
        });
      });
  };

}

export default BidController;
