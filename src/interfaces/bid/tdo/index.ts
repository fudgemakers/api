import { IsString } from 'class-validator';

class CreateBid {
  @IsString()
  public lotId: string | undefined = undefined;

  @IsString()
  public bid: string | undefined = undefined;

  @IsString()
  public userId: string | undefined = undefined;

}

export default CreateBid;