
import { BuildOptions, DataTypes, Model, Sequelize } from "sequelize";

export interface BidAttributes {
  lotId: number;
  bid: number;
  userId: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface BidModel extends Model<BidAttributes>, BidAttributes {};
export class Bid extends Model<BidModel, BidAttributes> {};

export type BidStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): BidModel;
};

export const BidFactory = (sequelize: Sequelize): BidStatic => {
  return sequelize.define("bid", {
    id: {
      type: new DataTypes.INTEGER(),
      autoIncrement: true,
      primaryKey: true,
    },
    lotId: {
      type: new DataTypes.INTEGER({ scale: 10 }),
      allowNull: false
    },
    bid: {
      type: new DataTypes.INTEGER({ scale: 10 }),
      allowNull: false
    },
    userId: {
      type: new DataTypes.INTEGER({ scale: 10 }),
      allowNull: false
    }
  },
  {
    tableName: 'bid'
  }) as BidStatic;
}
