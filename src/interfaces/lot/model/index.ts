import { BuildOptions, DataTypes, Model, Sequelize } from 'sequelize';
import { BidFactory } from '../../bid/model';
import { ImagesFactory } from '../../images/model';
import { sequalize } from '../../../sequalize';

export interface LotRequestAttributes {
  artist: string;
  category?: string;
  price: string;
  status: string;
  descriptionShort?: string;
  descriptionLong?: string;
  note?: string;
  closingAt: string;
}

export interface LotAttributes {
  artist: string;
  category: LotCategory;
  price: number;
  status: number;
  descriptionShort?: string;
  descriptionLong?: string;
  note?: string;
  closingAt: Date;
  createdAt?: Date;
  updatedAt?: Date;
}

export enum LotCategory {
  Internet = 'internet',
  Quality = 'quality',
}

export interface LotModel extends Model<LotAttributes>, LotAttributes {}
export class Lot extends Model<LotModel, LotAttributes> {}

export type LotStatic = typeof Model & {
  new (values?: Record<string, unknown>, options?: BuildOptions): LotModel
};

const LotFactory = (sequelize: Sequelize): LotStatic => {
  return sequelize.define('lot', {
      id: {
          type: new DataTypes.INTEGER(),
          autoIncrement: true,
          primaryKey: true,
      },
      artist: {
        type: new DataTypes.STRING(),
        allowNull: false
      },
      category: {
        type: new DataTypes.STRING(),
        allowNull: false
      },
      price: {
        type: new DataTypes.INTEGER(),
        defaultValue: 0,
        allowNull: false
      },
      status: {
        defaultValue: 0,
        type: new DataTypes.TINYINT(),
        allowNull: false,
      },
      descriptionShort: {
        type: new DataTypes.STRING()
      },
      descriptionLong: {
        type: new DataTypes.TEXT()
      },
      note: {
        type: new DataTypes.TEXT()
      },
      provisionSell: {
        type: new DataTypes.INTEGER()
      },
      provisionBuy: {
        type: new DataTypes.INTEGER()
      },
      costImages: {
        defaultValue: 40,
        type: new DataTypes.INTEGER()
      },
      costInsurance: {
        allowNull: false,
        defaultValue: 1.50,
        type: new DataTypes.FLOAT(10,2)
      },
      closingAt: {
        type: new DataTypes.DATE()
      },
      createdAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW,
      },
      updatedAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW,
      },
  }, {
    tableName: 'lot'
  }) as LotStatic;
}

export const LotModel = LotFactory(sequalize);
export const BidModel = BidFactory(sequalize);
export const ImagesModel = ImagesFactory(sequalize);


LotModel.hasMany(BidModel, { as: 'bids' });
BidModel.belongsTo(LotModel, {
    foreignKey: 'lotId',
    as: 'lot',
  });

LotModel.hasMany(ImagesModel, { as: 'images' });
ImagesModel.belongsTo(LotModel, {
  foreignKey: 'lotId',
  as: 'lot',
});
