import { IsString } from 'class-validator';

class CreateLot {
  @IsString()
  public artist: string | undefined = undefined;

  @IsString()
  public category: string | undefined = undefined;

  @IsString()
  public price: string | undefined = undefined;

  @IsString()
  public status: string | undefined = undefined;

  @IsString()
  public descriptionShort: string | undefined = undefined;

  @IsString()
  public descriptionLong: string | undefined = undefined;

  @IsString()
  public note: string | undefined = undefined;

  @IsString()
  public closingAt: string | undefined = undefined;
}

export default CreateLot;