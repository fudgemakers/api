import express, { NextFunction } from 'express';
import { Op } from 'sequelize';
import { LotAttributes, LotCategory, LotModel, LotRequestAttributes } from '../model';
import CreateLot from '../tdo';
import { settings } from '../../../settings';
import { HttpException, PostNotFoundException } from '../../../exceptions';
import { validationMiddleware } from '../../../middeware';

class LotController {
  public path = '/lot';
  public router = express.Router();

  constructor() {
    this.router.get(this.path, this.findAll);
    this.router.get(`${this.path}/published`, this.findAllPublished);
    this.router.get(`${this.path}/:id`, this.findOne);
    this.router.post(this.path, validationMiddleware(CreateLot), this.create);
    this.router.patch(`${this.path}/:id`, validationMiddleware(CreateLot, true), this.update);
    this.router.delete(`${this.path}/:id`, this.deleteOne);
    this.router.delete(`${this.path}`, this.deleteAll);
  }

  private findAll = (req: express.Request, res: express.Response, next: NextFunction) => {
    const conditions = req.query ?
      Object.entries(req.query).map(([key, value]) => {
        const c = (key === 'price' || key === 'status') ? { [Op.eq]: `${value}` } : { [Op.like]: `%${value}%` };
        return { [key]: c };
      }) : null;
    const flattened = conditions ? conditions.map(c => c) : {};
      LotModel.findAll({ where: flattened, include: ['bids', 'images'] })
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          next(new HttpException(404, 'Posts not found'));
        }
      })
      .catch(error => {
        res.status(500).send({
          message:
            error.message || 'Some error occurred while retrieving Lots.'
        });
      });
  };

  private findOne = (req: express.Request, res: express.Response, next: NextFunction) => {

    const { id } = req.params;

    LotModel.findByPk(id, { include: ['bids', 'images'] })
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          next(new PostNotFoundException(id));
        }
      })
      .catch(error => {
        res.status(500).send({
          status: 'error',
          message: `Error ${ error }`
        });
      });
  };

  private update = (req: express.Request, res: express.Response, next: NextFunction) => {
    const id = req.params.id;
    const category = (req.body.category as string | undefined);
    if (!validateCategory(category)) {
      res.status(404).send({
        status: 'error',
        message: `Lot category must be of type '${ LotCategory.Internet}' | '${ LotCategory.Quality }'`
      });
      return;
    };

    LotModel.update(req.body, {
      where: { id }
    })
    .then((post) => {
      if(post) {
        res.send({
          status: `ok`,
          message: `Lot ${ id } updated successfully`
        });
      } else {
        next(new PostNotFoundException(id));
      }
    });
  };

  private create = (req: express.Request, res: express.Response) => {

    const lot = createLot(req.body);

    LotModel.create(lot)
      .then(data => {
        res.send(data);
      })
      .catch((error: { message: string }) => {
        res.status(500).send({
          message:
            error.message || 'Some error occurred while creating the Lot.'
        });
      });
  };

  private deleteOne = (req: express.Request, res: express.Response, next: NextFunction) => {
    const id = req.params.id;

    LotModel.destroy({
      where: { id }
    })
    .then((post) => {
      if(post) {
        res.send({
          status: post,
          message: `Lot ${id} deleted successfully`
        });
      } else {
        next(new PostNotFoundException(id));
      }
    });
  };

  private deleteAll = (req: express.Request, res: express.Response, next: NextFunction) => {
    LotModel.destroy({
      where: {},
      truncate: false
    })
    .then((post) => {
      if(post) {
        res.send({
          status: post,
          message: `Lots deleted successfully`
        });
      } else {
        next(new HttpException(404, 'Lots not found'));
      }
    });
  };

  private findAllPublished = (req: express.Request, res: express.Response, next: NextFunction) => {
    LotModel.findAll({ where: { status: 1 } })
      .then(data => {
        if(data) {
          res.send(data);
        } else {
          next(new HttpException(404, 'Lots not found'));
        }
    });
  }

}

const validateCategory = (category: string | undefined) => {
  return category === LotCategory.Internet || category === LotCategory.Quality;
}

const getValidCategory = (category: string | undefined) => {
  switch (category) {
    case 'internet':
      return LotCategory.Internet;
    case 'quality':
      return LotCategory.Quality;
    default:
      return LotCategory.Internet;
  }
}

const createLot = (lot: LotRequestAttributes): LotAttributes => {
  const { artist, category, price, status, descriptionShort, descriptionLong, note, closingAt } = lot;

  return {
    artist,
    category: getValidCategory(category),
    price: parseInt(price, 10),
    status: status ? parseInt(status, 10) : settings.DEFAULT_STATUS,
    descriptionShort,
    descriptionLong,
    note,
    closingAt: new Date(closingAt)
  };
}

export default LotController;