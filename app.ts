import express from 'express';
import cors from 'cors';
import BidController from './src/interfaces/bid/controller';
import LotController from './src/interfaces/lot/controller';
import ImagesController from './src/interfaces/images/controller';
import { errorMiddleware } from './src/middeware';
import 'reflect-metadata';

class App {
  public app: express.Application;
  public port: number;
  public corsOptions = {
    origin: 'http://localhost:5000'
  };

  constructor(controllers: (BidController | LotController | ImagesController)[], port: number) {
    this.app = express();
    this.port = port;

    this.app.get("/", (req, res) => {
      res.json({ message: "Welcome to vonSchéele API." });
    });
    this.initializeMiddleWares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  private initializeMiddleWares() {
    this.app.use(cors(this.corsOptions));
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(express.json());
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeControllers(controllers: (BidController | LotController | ImagesController)[]) {
    controllers.map(c => {
      this.app.use('/', c.router);
    });
  };

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`Àpp listeing on port ${this.port}`);
    })
  }
}

export default App;