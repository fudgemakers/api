import App from './app';
import LotController from './src/interfaces/lot/controller';
import BidController from './src/interfaces/bid/controller';
import ImagesController from './src/interfaces/images/controller';
import { sequalize } from './src/sequalize';


const app = new App(
  [
    new LotController(),
    new BidController(),
    new ImagesController()
  ],
  5000
);

sequalize.sync();
// // drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

app.listen();
